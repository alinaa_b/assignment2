
const FIRST_NAME = "Alina";
const LAST_NAME = "Bica";
const GRUPA = "1075";

/**
 * Make the implementation here
 */



function initCaching() {
    var cache = {
        magnificentObject: {},
        pageAccessCounter: function  (page) {
        	if(page) {
        		let key = page.toLowerCase();
        		this.magnificentObject[key] ? this.magnificentObject[key]++ : this.magnificentObject[key] = 1; 
        	} else {
        		this.magnificentObject['home'] ? this.magnificentObject['home']++ : this.magnificentObject['home'] = 1;
        	}
        },
        getCache: function  () {
        	return this.magnificentObject;
        },
    };

    return cache
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

